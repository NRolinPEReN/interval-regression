
Allows to do intervall regression. Go the notebook interval_regression to see how to use it, and intervall_regression-math to have a small explaination on the maths behind it.
Freely inspired by https://github.com/jamesdj/tobit

TODO : add consistency to the handling of the data + remove all the copy-paste on the notebook