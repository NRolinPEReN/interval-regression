import warnings

import numpy as np
import scipy.stats
from scipy.optimize import minimize
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error


USABLE_METHODS = ["Nelder-Mead", "Powell", "CG", "BFGS", "L-BFGS-B", "TNC", "COBYLA", "SLSQP"]


def get_index_vector(y_orig, intervals):
    """
    function usefull if you want to generate fake data and censor them
    Returns an array with the index of the interval in which the y value was
    """
    def select_bound(y, lower_bound, upper_bound):
        if lower_bound is None and upper_bound is None:
            return np.logical_or(y <= 0, y >=0)
        lower_bound_array = y >= lower_bound if lower_bound is not None else True
        upper_bound_array = y < upper_bound if upper_bound is not None else True
        interval_array = np.logical_and(lower_bound_array, upper_bound_array)
        return interval_array

    y_index = np.zeros(len(y_orig), dtype=int)
    for index, (lower_bound, upper_bound) in intervals.items():
        y_index[select_bound(y_orig, lower_bound, upper_bound)] = index
    return y_index


def censor_log_likelihood(x_with_intercept, y_index, intervals, params, alternative_fit=False):
    """
    Calculate the log likelyhood of the dataset given the params
    :param data_intervals:
    :param intervals:
    :param params:
    :param alternative_fit: if set to true, will suppose a noise on the regression coefficient and not just a random noise
    :return:
    """
    coeffs, variance = params[:-1], params[-1]
    log_likelyhood_sum = 0

    for interval_index, (lower_bound, upper_bound) in intervals.items():
        lower_bound = lower_bound if lower_bound is not None else -np.inf
        upper_bound = upper_bound if upper_bound is not None else np.inf
        points_in_interval = y_index == interval_index
        if np.any(points_in_interval) is False:
            continue
        x_in_interval = x_with_intercept[points_in_interval]
        y_prediction = np.dot(x_in_interval, coeffs)
        lower_diff = (y_prediction - lower_bound) / variance
        upper_diff = (y_prediction - upper_bound) / variance
        if alternative_fit:  # works only for one feature atm.
            lower_diff = lower_diff / (x_in_interval[:, 1] + np.finfo('float').resolution)  # avoid division by zero issues
            upper_diff = upper_diff / (x_in_interval[:, 1] + np.finfo('float').resolution)
        lower_diff_norm = scipy.stats.norm.cdf(lower_diff)
        upper_diff_norm = scipy.stats.norm.cdf(upper_diff)
        # in case of really unprobable points, likelihood could be lower than float precision so we cap it)
        likelyhood = np.maximum(lower_diff_norm - upper_diff_norm, np.finfo('float').resolution)
        log_likelyhood_sum += np.log(likelyhood).sum()
    return log_likelyhood_sum


def censor_log_likelihood_jacobian(x_with_intercept, y_index, intervals, params, alternative_fit=False):
    """ calculate the derivative of the likeliyhood. see the notebook for the math formula
    It doesn't work so there must be an error here
    """
    coeffs, variance = params[:-1], params[-1]

    beta_jacobian = np.zeros(len(coeffs))  # we'll see when beta is more than one
    sigma_jacobian = 0

    for interval_index, (lower_bound, upper_bound) in intervals.items():
        lower_bound = lower_bound if lower_bound is not None else -np.inf
        upper_bound = upper_bound if upper_bound is not None else np.inf
        points_in_interval = y_index == interval_index
        if np.any(points_in_interval) is False:
            continue
        x_in_interval = x_with_intercept[points_in_interval]
        y_prediction = np.dot(x_in_interval, coeffs)
        lower_diff = (y_prediction - lower_bound) / variance
        upper_diff = (y_prediction - upper_bound) / variance
        if alternative_fit:  # works only for one feature atm.
            lower_diff = lower_diff / (x_in_interval[:, 1] + np.finfo('float').resolution)  # avoid division by zero issues
            upper_diff = upper_diff / (x_in_interval[:, 1] + np.finfo('float').resolution)
        lower_diff_norm = scipy.stats.norm.cdf(lower_diff)
        upper_diff_norm = scipy.stats.norm.cdf(upper_diff)
        # in case of really unprobable points, likelihood could be lower than float precision so we cap it)
        likelihood = np.maximum(lower_diff_norm - upper_diff_norm, np.finfo('float').resolution)

        lower_diff_norm_derivative = scipy.stats.norm.pdf(lower_diff)
        if lower_bound == -np.inf:
            lower_diff = np.zeros(len(lower_diff))  # derivatives are still 0 at infinity
        sigma_lower_diff_norm_derivative = np.multiply(lower_diff, lower_diff_norm_derivative)

        if upper_bound == np.inf:
            upper_diff = np.zeros(len(upper_diff))  # derivatives are still 0 at infinity
        upper_diff_norm_derivative = scipy.stats.norm.pdf(upper_diff)
        sigma_upper_diff_norm_derivative = np.multiply(upper_diff, upper_diff_norm_derivative)
        likelihood_derivative = np.maximum(
            lower_diff_norm_derivative - upper_diff_norm_derivative,
            np.finfo('float').resolution
        )
        log_likelihood_derivative = likelihood_derivative / likelihood

        beta_gradient = np.dot(log_likelihood_derivative, x_in_interval / variance)
        beta_jacobian += beta_gradient

        sigma_likelihood_derivative = (sigma_lower_diff_norm_derivative - sigma_upper_diff_norm_derivative) / variance
        sigma_gradient = sigma_likelihood_derivative / likelihood
        sigma_jacobian += sigma_gradient.sum()

    return np.append(beta_jacobian, sigma_jacobian)


def get_data_midpoints(y_index, intervals):
    """
    Interpolates the data with the midpoints
    """
    def midpoint(pt1, pt2):
        if pt1 is None:
            return pt2
        if pt2 is None:
            return pt1
        return (pt1 + pt2)/2

    intervals_midpoints = {
        index : midpoint(lower_bound, upper_bound)
        for index, (lower_bound, upper_bound) in intervals.items()
    }
    get_midpoints = np.vectorize(intervals_midpoints.get)
    return get_midpoints(y_index)


class IntervalModel:
    def __init__(self, fit_intercept=True, verbose=True, extra_verbose=False):
        self.fit_intercept = fit_intercept
        self.ols_coef_ = None
        self.ols_intercept = None
        self.ols_sigma = None
        self.ols_predict = None
        self.coef_ = None
        self.intercept_ = None
        self.sigma_ = None
        self.verbose = verbose
        self.extra_verbose = extra_verbose

    def fit(self, x, y_index, intervals, method='Powell', alternative_fit=False, fast_slove=False):
        """
        Fit a maximum-likelihood Tobit regression
        :param data_intervals: data in the form {index_bound : x (Pandas Series)}

        Pandas DataFrame (n_samples, n_features): Data
        :param intervals: dict of the form {index: (lower_bound, upper_bound)}, with None acceptable as a bound (for infinity)
        :return:
        """
        y_midpoints = get_data_midpoints(y_index, intervals)
        number_of_points = x.shape[0]
        x_with_intercept = np.append(np.ones((number_of_points, 1)), x, axis=1)

        if alternative_fit:  # we take a double log because the data is very spread and high values takes all the cost
            regression_on_midpoints = LinearRegression(fit_intercept=False).fit(np.log(x_with_intercept), np.log(y_midpoints))
        else:
            regression_on_midpoints = LinearRegression(fit_intercept=False).fit(x_with_intercept, y_midpoints)

        init_coeffs = regression_on_midpoints.coef_
        y_prediction = regression_on_midpoints.predict(x_with_intercept)
        init_variance = np.sqrt(np.var(y_midpoints - y_prediction))

        if alternative_fit:
            init_coeffs = np.exp(init_coeffs)
            y_prediction = np.exp(regression_on_midpoints.predict(np.log(x_with_intercept)))
            init_variance = np.sqrt(np.var((y_midpoints - y_prediction)/(x_with_intercept[:, 1] + np.finfo('float').resolution)))

        params0 = np.append(init_coeffs, init_variance)

        def get_result(initialization, algorithms=USABLE_METHODS):
            result_dict = {
                method: minimize(
                    lambda params: - censor_log_likelihood(
                        x_with_intercept, y_index, intervals, params, alternative_fit=alternative_fit,
                    ),
                    initialization,
                    jac=lambda params: - censor_log_likelihood_jacobian(
                        x_with_intercept, y_index, intervals, params, alternative_fit=alternative_fit
                    ),
                    method=method, options={'disp': self.extra_verbose})
                for method in algorithms
            }
            return min((results for results in result_dict.values()), key=lambda solution: solution.fun)


        if fast_slove is True:
            result = get_result(initialization=params0, algorithms=[method])
        else:  # we do a first pass with all algorithms to find good initialization params
            with warnings.catch_warnings():  # we get a lot of warnings because minimisation takes different entries
                warnings.simplefilter("ignore")
                result_for_better_initialization = get_result(initialization=params0)
                result = get_result(initialization=result_for_better_initialization.x)

        if self.verbose:
            print(result)
            self.ols_intercept, self.ols_coef_, self.ols_sigma = init_coeffs[0], init_coeffs[1:], init_variance
            self.ols_predict = lambda x_no_intercept: init_coeffs[0] +  x_no_intercept * init_coeffs[1:]
        if self.fit_intercept:
            self.intercept_ = result.x[0]
            self.coef_ = result.x[1:-1]
        self.sigma_ = result.x[-1]
        return self

    def predict(self, x):
        return self.intercept_ + np.dot(x, self.coef_)

    def score(self, x, y, scoring_function=mean_absolute_error):
        y_pred = np.dot(x, self.coef_)
        return scoring_function(y, y_pred)
