# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# io.open is needed for projects that support Python 2.7
# It ensures open() defaults to text mode with universal newlines,
# and accepts an argument to specify the text encoding
# Python 3 only projects can skip this import


setup(
    name='interval_regression',
    version='1.0',
    description='Allow to do a censored regression',
    author='PEReN',
    author_email='peren@finances.gouv.fr',
    # When your source code is in a subdirectory under the project root, e.g.
    # `src/`, it is necessary to specify the `package_dir` argument.
    package_dir={},
    packages=find_packages(),  # packages=find_packages(where='interval_regression'),

)